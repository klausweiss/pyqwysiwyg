from abc import ABCMeta, abstractmethod

from Observers import Observer


class TextEditObserver(Observer):
    __metaclass__ = ABCMeta

    def __init__(self, strategy, editor):
        super(TextEditObserver, self).__init__(strategy)
        self.editor = editor

    @abstractmethod
    def update(self):
        return super(TextEditObserver, self).update()
