from abc import ABCMeta

from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import QTextEdit


class EditorUtil:
    """
    Modifies cursor in specific way.
    """
    __metaclass__ = ABCMeta

    @staticmethod
    def execute(cursor):
        raise NotImplementedError("Utility not implemented")

    @staticmethod
    def detect_state(editor, condition):
        """
        Returns True if passed condition is met.
        :param editor: Editor with selected text/cursor.
        :param condition: Function with editor as argument.
        :return: State of condition.
        """
        assert isinstance(editor, QTextEdit)

        cursor = editor.textCursor()
        assert isinstance(cursor, QTextCursor)

        # Selected right-to-left
        selected_from_right = cursor.anchor() > cursor.position()

        # Style of first character (after first character).
        if selected_from_right:
            cursor.movePosition(QTextCursor.NextCharacter, QTextCursor.KeepAnchor)

        # Detect if passed condition was met.
        condition_met = condition(cursor.charFormat())

        # Undo previous selected-from-right 'hack'.
        if selected_from_right:
            cursor.movePosition(QTextCursor.PreviousCharacter, QTextCursor.KeepAnchor)

        return condition_met

    @staticmethod
    def state_ok(editor):
        """
        :param editor: Editor to be checked.
        :return: Returns true if detected_state meets condition.
        """
        raise NotImplementedError("Implement state-checking method")
