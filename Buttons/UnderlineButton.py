from Buttons.SingleLetterButton import SingleLetterButton


class UnderlineButton(SingleLetterButton):
    """
    ToolbarButton with underlined U.
    """
    icon_letter = "U"

    def setup_icon(self):
        """
        Sets up button icon properties:
        - text (letter)
        - font
        """
        # Icon text
        self.setText(UnderlineButton.icon_letter)

        # Icon text font
        self.font.setUnderline(True)
