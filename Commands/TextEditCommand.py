from PyQt5 import QtWidgets
from abc import ABCMeta

from Commands import Command


class TextEditCommand(Command):
    """
    Abstract class for processing commands that edit TextEdit.
    """
    __metaclass__ = ABCMeta

    def __init__(self, editor):
        assert isinstance(editor, QtWidgets.QTextEdit)
        self.editor = editor

    def execute(self):
        # Leave abstract as it was.
        super(TextEditCommand, self).execute()
