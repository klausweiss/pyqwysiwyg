from PyQt5 import QtWidgets, QtCore


class ToolbarButton(QtWidgets.QPushButton):
    """
    Default button used in Toolbar.
    32x32 square.
    """
    # Rectangle 32x32 by default.
    button_geometry = QtCore.QRect(0, 0, 32, 32)
    button_size = QtCore.QSize(32, 32)

    def __init__(self):
        super(ToolbarButton, self).__init__()

        self.setup_properties()

    def setup_properties(self):
        """
        Sets up default properties:
        - size
        - checkable
        """
        self.setGeometry(ToolbarButton.button_geometry)
        self.setMinimumSize(ToolbarButton.button_size)
        self.setMaximumSize(ToolbarButton.button_size)
        self.setCheckable(True)
