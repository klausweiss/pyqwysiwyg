from PyQt5 import QtGui

from Buttons.ToolbarButton import ToolbarButton

from abc import ABCMeta, abstractmethod


class SingleLetterButton(ToolbarButton):
    """
    ToolbarButton with single letter (string) as icon.
    """
    __metaclass__ = ABCMeta

    def __init__(self):
        super(SingleLetterButton, self).__init__()

        # Icon font
        self.font = QtGui.QFont()

        # Setup icon.
        self.setup_icon()

        # Apply font settings.
        self.setFont(self.font)

    @abstractmethod
    def setup_icon(self):
        raise NotImplementedError("setup_icon not implemented")
