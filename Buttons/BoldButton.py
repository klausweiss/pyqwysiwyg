from Buttons.SingleLetterButton import SingleLetterButton


class BoldButton(SingleLetterButton):
    """
    ToolbarButton with bold B.
    """
    icon_letter = "B"

    def setup_icon(self):
        """
        Sets up button icon properties:
        - text (letter)
        - font
        """
        # Icon text
        self.setText(BoldButton.icon_letter)

        # Icon text font
        self.font.setBold(True)
        self.font.setWeight(75)
