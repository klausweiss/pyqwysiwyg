from abc import ABCMeta

from PyQt5.QtWidgets import QTextEdit

from EditorUtilities.EditorUtil import EditorUtil


class EditorUnderlineUtil(EditorUtil):
    """
    Underlines editor's cursor/selected text.
    """
    __metaclass__ = ABCMeta

    @staticmethod
    def execute(editor):
        """
        Underlines selected text in editor.
        :param editor: Editor to underline text in.
        """
        assert isinstance(editor, QTextEdit)

        # Detect if need to make text underline or normal.
        make_italic = not EditorUnderlineUtil.state_ok(editor)

        # The actual set underline.
        if make_italic:
            editor.setFontUnderline(True)
        else:
            editor.setFontUnderline(False)

    @staticmethod
    def state_ok(editor):
        """
        Returns True if selected text is underlined.
        :param editor: Editor with selected text/cursor.
        :return: Is selected text underlined.
        """
        assert isinstance(editor, QTextEdit)

        return EditorUtil.detect_state(editor,
                                       lambda char_format: char_format.fontUnderline()
                                       )
