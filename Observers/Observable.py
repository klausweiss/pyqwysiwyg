from abc import ABCMeta, abstractmethod


class Observable:
    """
    Abstract class for objects that can be observed by Observer.
    """
    __metaclass__ = ABCMeta

    @abstractmethod
    def attach(self, observer):
        """
        Attaches observer to observable object.
        :param observer: Observer object to be attached.
        """
        raise NotImplementedError("attach not implemented")

    @abstractmethod
    def detach(self, observer):
        """
        Detaches observer to observable object.
        :param observer: Observer object to be detached.
        """
        raise NotImplementedError("detach not implemented")

    @abstractmethod
    def notify(self):
        """
        Notifies all attached observers of event that happened.
        """
        raise NotImplementedError("notify not implemented")
