from PyQt5.QtWidgets import QTextEdit

from Commands.TextEditCommand import TextEditCommand
from EditorUtilities import EditorUnderlineUtil


class UnderlineCommand(TextEditCommand):
    """
    Responsible for making text/actual cursor (not) italic.
    """
    def __init__(self, editor):
        # TextEditCommand constructor.
        super(UnderlineCommand, self).__init__(editor=editor)

    def execute(self):
        """
        Underline text/cursor in TextEdit.
        """
        assert isinstance(self.editor, QTextEdit)
        EditorUnderlineUtil.execute(self.editor)
