from abc import ABCMeta

from PyQt5.QtWidgets import QTextEdit

from EditorUtilities.EditorUtil import EditorUtil


class EditorItalicUtil(EditorUtil):
    """
    Makes editor's cursor/selected text italic.
    """
    __metaclass__ = ABCMeta

    @staticmethod
    def execute(editor):
        """
        Makes selected text in editor italic.
        :param editor: Editor to make text italic in.
        """
        assert isinstance(editor, QTextEdit)

        # Detect if need to make text bold or normal.
        make_italic = not EditorItalicUtil.state_ok(editor)

        # The actual set italic.
        if make_italic:
            editor.setFontItalic(True)
        else:
            editor.setFontItalic(False)

    @staticmethod
    def state_ok(editor):
        """
        Returns True if selected text is italic.
        :param editor: Editor with selected text/cursor.
        :return: Is selected text italic.
        """
        assert isinstance(editor, QTextEdit)

        return EditorUtil.detect_state(editor,
                                       lambda char_format: char_format.fontItalic()
                                       )
