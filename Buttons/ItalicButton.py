from Buttons.SingleLetterButton import SingleLetterButton


class ItalicButton(SingleLetterButton):
    """
    ToolbarButton with italic I.
    """
    icon_letter = "I"

    def setup_icon(self):
        """
        Sets up button icon properties:
        - text (letter)
        - font
        """
        # Icon text
        self.setText(ItalicButton.icon_letter)

        # Icon text font
        self.font.setItalic(True)
