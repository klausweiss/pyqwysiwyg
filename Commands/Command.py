from abc import ABCMeta, abstractmethod


class Command:
    __metaclass__ = ABCMeta

    class Trigger:
        """
        Used to determine how the command was executed.
        """
        SHORTCUT = 1
        BUTTON = 2

    @abstractmethod
    def execute(self):
        """
        Execute command.
        """
        raise NotImplementedError("execute not implemented")
