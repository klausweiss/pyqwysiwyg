from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QSizePolicy

from Buttons import BoldButton, ItalicButton, UnderlineButton
from Commands import BoldCommand, ItalicCommand, UnderlineCommand
from Commands.Triggers import TextEditCommandTrigger
from EditorUtilities import EditorBoldUtil, EditorItalicUtil, EditorUnderlineUtil
from Observers import Observable, Observer, TextEditButtonObserver


class Toolbar(QtWidgets.QWidget, Observable):
    """
    Widget linked with TextEdit.
    Responsible for triggering when to change something in text.
    """

    def __init__(self, editor, parent_container=None):
        super(Toolbar, self).__init__(parent_container)

        # UI

        # Layout
        self.container = QtWidgets.QHBoxLayout()

        # Buttons
        self.bold_button = BoldButton()
        self.italic_button = ItalicButton()
        self.underline_button = UnderlineButton()

        # Editor
        self.editor = editor

        # COMMANDS

        # Triggers
        self.bold_command_trigger = TextEditCommandTrigger(
            BoldCommand(self.editor),
            button=self.bold_button)
        self.italic_command_trigger = TextEditCommandTrigger(
            ItalicCommand(self.editor),
            button=self.italic_button)
        self.underline_command_trigger = TextEditCommandTrigger(
            UnderlineCommand(self.editor),
            button=self.underline_button)

        # OBSERVERS
        self.observers = list()
        self.setup_observers()

        self.setup_ui()
        self.setup_styles()

    def setup_ui(self):
        """
        Sets up UI structure.
        """
        # INITIALIZE

        # Spacer
        spacer = QtWidgets.QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Maximum)

        # STRUCTURE

        self.container.addWidget(self.bold_button)
        self.container.addWidget(self.italic_button)
        self.container.addWidget(self.underline_button)
        self.container.addSpacerItem(spacer)

        # Layout

        self.setLayout(self.container)

    def setup_styles(self):
        """
        Sets up UI styles.
        """
        self.container.setContentsMargins(0, 0, 0, 0)

    # OBSERVER

    def setup_observers(self):
        """
        Sets up observers for text states.
        """
        self.attach(TextEditButtonObserver(EditorBoldUtil.state_ok, self.editor, self.bold_button))
        self.attach(TextEditButtonObserver(EditorItalicUtil.state_ok, self.editor, self.italic_button))
        self.attach(TextEditButtonObserver(EditorUnderlineUtil.state_ok, self.editor, self.underline_button))

    def detach(self, observer):
        """
        Detaches observer.
        :param observer: Observer to be detached.
        """
        self.observers.remove(observer)

    def attach(self, observer):
        """
        Attaches observer.
        :param observer: Observer to be attached.
        """
        self.observers.append(observer)

    def notify(self):
        """
        Notifies all observers of event that occurred.
        """
        for observer in self.observers:
            assert isinstance(observer, Observer)
            observer.update()

    def connect_observer_event(self, event):
        """
        Connects (Qt) event to notify observers.
        :param event: Event to be connected.
        """
        event.connect(self.notify)
