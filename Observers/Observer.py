from abc import ABCMeta, abstractmethod


class Observer:
    """
    Abstract class for object that can observe other object
    and perform operations based on observed changes according to its strategy.
    """
    __metaclass__ = ABCMeta

    def __init__(self, strategy):
        self.strategy = strategy

    @abstractmethod
    def update(self):
        """
        Perform operations based on Observer's strategy.
        """
        raise NotImplementedError("Observer's `update` method not implemented")
