from abc import ABCMeta

from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QTextEdit

from EditorUtilities.EditorUtil import EditorUtil


class EditorBoldUtil(EditorUtil):
    """
    Makes editor's cursor/selected text bold.
    """
    __metaclass__ = ABCMeta

    @staticmethod
    def execute(editor):
        """
        Makes selected text in editor bold.
        :param editor: Editor to make text bold in.
        """
        assert isinstance(editor, QTextEdit)

        # Detect if need to make text bold or normal.
        make_bold = not EditorBoldUtil.state_ok(editor)

        # The actual set weight.
        if make_bold:
            editor.setFontWeight(QFont.Bold)
        else:
            editor.setFontWeight(QFont.Normal)

    @staticmethod
    def state_ok(editor):
        """
        Returns True if selected text is bold.
        :param editor: Editor with selected text/cursor.
        :return: Is selected text bold.
        """
        assert isinstance(editor, QTextEdit)

        return EditorUtil.detect_state(editor,
                                       lambda char_format:
                                       char_format.fontWeight() in [QFont.Bold, QFont.DemiBold, QFont.Black]
                                       )
