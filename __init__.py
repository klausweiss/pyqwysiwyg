# Imported as module
if __name__ != "__main__":
    from .PyQWYSIWYG import PyQWYSIWYG
# Tested as standalone application
else:
    from PyQt5 import QtWidgets
    import sys
    from PyQWYSIWYG import PyQWYSIWYG

    def setup_ui(window):
        """
        Sets up window UI.
        :param window: window to set up UI.
        """
        assert isinstance(window, QtWidgets.QMainWindow)
        # Default window size.
        window.resize(1000, 500)

        # Central widget
        container = QtWidgets.QWidget(window)
        window.setCentralWidget(container)
        # Container
        layout = QtWidgets.QGridLayout(container)
        # WYSiWYG widget
        wysiwyg = PyQWYSIWYG()

        # Adding wysiwyg to layout structure
        layout.addWidget(wysiwyg)
        wysiwyg.setFocus()

        # Center window.
        window.move(QtWidgets.QApplication.desktop().screen().rect().center()-window.rect().center())


    # New application
    app = QtWidgets.QApplication(sys.argv)
    # Application window
    MainWindow = QtWidgets.QMainWindow()
    MainWindow.show()
    # UI
    setup_ui(MainWindow)

    sys.exit(app.exec_())
