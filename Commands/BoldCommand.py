from PyQt5.QtWidgets import QTextEdit

from Commands.TextEditCommand import TextEditCommand
from EditorUtilities import EditorBoldUtil


class BoldCommand(TextEditCommand):
    """
    Responsible for making text/actual cursor (not) bold.
    """
    def __init__(self, editor):
        # TextEditCommand constructor.
        super(BoldCommand, self).__init__(editor=editor)

    def execute(self):
        """
        Make text/cursor in TextEdit bold.
        """
        assert isinstance(self.editor, QTextEdit)
        EditorBoldUtil.execute(self.editor)
