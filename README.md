# PyQWYSIWYG

PyQWYSIWYG is a WYSIWYG editor for PyQt5. It allows basic rich text editing
and is easily-extensible.

# API
- **setFocus**: sets focus on editor
- **setHtml**: sets editor's HTML
- **toHtml**: returns editor's HTML
- **toPlainText**: returns editor's content as plain text

# Shortcuts
- **ctrl+b**: bold
- **ctrl+i**: italic
- **ctrl+u**: underline

# License
The code is shared under MIT license - see `LICENSE` for details.