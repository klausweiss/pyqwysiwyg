from PyQt5.QtWidgets import QPushButton

from Observers import TextEditObserver


class TextEditButtonObserver(TextEditObserver):
    def __init__(self, strategy, editor, button):
        super(TextEditButtonObserver, self).__init__(strategy, editor)
        self.button = button

    def update(self):
        """
        Toggles button based on text format.
        """
        new_state = self.strategy(self.editor)
        assert isinstance(self.button, QPushButton)
        if self.button.isChecked() is not new_state:
            self.button.setChecked(new_state)
