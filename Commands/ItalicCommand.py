from PyQt5.QtWidgets import QTextEdit

from Commands.TextEditCommand import TextEditCommand
from EditorUtilities import EditorItalicUtil


class ItalicCommand(TextEditCommand):
    """
    Responsible for making text/actual cursor (not) italic.
    """
    def __init__(self, editor):
        # TextEditCommand constructor.
        super(ItalicCommand, self).__init__(editor=editor)

    def execute(self):
        """
        Make text/cursor in TextEdit italic.
        """
        assert isinstance(self.editor, QTextEdit)
        EditorItalicUtil.execute(self.editor)
