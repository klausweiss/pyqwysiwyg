from PyQt5 import QtWidgets
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QShortcut

from Toolbar import Toolbar


class PyQWYSIWYG(QtWidgets.QWidget):
    """
    WYSiWYG widget
    """
    def __init__(self, parent_container=None):
        super().__init__(parent_container)

        # UI

        # Layout
        self.container = QtWidgets.QGridLayout(self)

        # Basic elements.
        self.editor = QtWidgets.QTextEdit()
        self.toolbar = Toolbar(self.editor)

        # SETUP

        # UI
        self.setup_ui()
        self.setup_styles()

        # Events
        self.setup_shortcuts()
        self.setup_events()

    # SET UP

    def setup_ui(self):
        """
        Initializes UI elements, adds items to containers.
        """
        # Add layout children.
        self.container.addWidget(self.toolbar)
        self.container.addWidget(self.editor)

    def setup_events(self):
        """
        Initializes UI elements' events.
        """
        # TextEdit events.
        # Connects 'char format changed' event to observers.
        self.toolbar.connect_observer_event(self.editor.currentCharFormatChanged)

    def setup_shortcuts(self):
        """
        Sets up keyboard shortcut for text actions (toolbar buttons).
        """
        # Bold shortcut.
        # Ctrl + b
        bold_shortcut = QShortcut(QKeySequence("Ctrl+b"), self)
        self.toolbar. bold_command_trigger.set_shortcut(bold_shortcut)

        # Italic shortcut.
        # Ctrl + i
        italic_shortcut = QShortcut(QKeySequence("Ctrl+i"), self)
        self.toolbar.italic_command_trigger.set_shortcut(italic_shortcut)

        # Underline shortcut.
        # Ctrl + u
        underline_shortcut = QShortcut(QKeySequence("Ctrl+u"), self)
        self.toolbar.underline_command_trigger.set_shortcut(underline_shortcut)

    def setup_styles(self):
        """
        Sets up styles for UI elements.
        """
        self.container.setContentsMargins(0, 0, 0, 0)

    # OVERRIDDEN

    def setFocus(self, reason=None):
        """
        Sets focus on editor.
        :param reason:
        """
        # Pass focus to editor
        return self.editor.setFocus()

    # GETTERS

    def toHtml(self):
        """
        :return Contents of editor as HTML.
        """
        return self.editor.toHtml()

    def toPlainText(self):
        """
        :return Contents of editor as plain text.
        """
        return self.editor.toPlainText()

    # SETTERS

    def setHtml(self, html):
        """
        Sets HTML of editor widget.
        :param html: HTML to be set
        """
        self.editor.setHtml(html)
