from PyQt5 import QtWidgets

from Commands import TextEditCommand
from Commands.Triggers.CommandTrigger import CommandTrigger


class TextEditCommandTrigger(CommandTrigger):

    def __init__(self, command, button=None, shortcut=None):
        """
        :param command: Command to be executed
        :param button: button that triggers command
        :param shortcut: shortcut that triggers command
        """
        super(TextEditCommandTrigger, self).__init__(command, button, shortcut)

    def execute_button(self):
        """
        Executed when button was clicked.
        """
        assert isinstance(self.command, TextEditCommand)
        self.command.editor.setFocus()

    def execute_shortcut(self):
        """
        Executed when shortcut was called.
        """
        if self.button is not None:
            assert isinstance(self.button, QtWidgets.QPushButton)
            self.button.toggle()
