from PyQt5 import QtWidgets
from abc import ABCMeta, abstractmethod

from PyQt5.QtWidgets import QPushButton

from Commands import Command


class CommandTrigger:
    """
    Abstract class for triggering commands (button/shortcut events).
    """

    __metaclass__ = ABCMeta

    class Trigger:
        """
        Used to determine how the command was executed.
        """
        SHORTCUT = 1
        BUTTON = 2

    def __init__(self, command, button=None, shortcut=None):
        """
        :param command: Command to be executed
        :param button: button that triggers command
        :param shortcut: shortcut that triggers command
        """
        assert isinstance(command, Command)
        self.command = command
        self.button = None
        self.shortcut = None

        # Set self.button and bind its 'clicked' event.
        if button is not None:
            self.set_button(button)
        # Set self.shortcut and bind its 'activated' event.
        if shortcut is not None:
            self.set_shortcut(shortcut)

    def set_shortcut(self, shortcut):
        """
        Binds shortcut to execute method.
        :param shortcut: QShortcut to be bound.
        """
        assert isinstance(shortcut, QtWidgets.QShortcut)
        self.shortcut = shortcut

        # Bind shortcut 'activated' event with execute.
        self.shortcut.activated.connect(
            lambda: self.execute(CommandTrigger.Trigger.SHORTCUT)
        )

    def set_button(self, button):
        """
        Binds button's 'clicked' event to execute method.
        :param button: ToolbarButton whose event to be bound.
        """
        assert isinstance(button, QPushButton)
        self.button = button

        # Bind button 'clicked' event with execute.
        self.button.clicked.connect(
            lambda: self.execute(CommandTrigger.Trigger.BUTTON)
        )

    def execute(self, trigger):
        """
        Execute command (depending on the trigger).
        :param trigger:
        """
        if trigger is CommandTrigger.Trigger.BUTTON:
            self.execute_button()
        else:
            self.execute_shortcut()

        self.command.execute()

    @abstractmethod
    def execute_button(self):
        """
        Executed when button was clicked.
        """
        raise NotImplementedError("Button event not implemented")

    @abstractmethod
    def execute_shortcut(self):
        """
        Executed when shortcut was called.
        """
        raise NotImplementedError("Shortcut event not implemented")
